token="my-token"
gitlab_runner_name="my-runner"

gitlab-runner register --non-interactive --name $gitlab_runner_name --url http://gitlab.example.com --registration-token $token --executor docker --docker-image docker:18.03.1-ce
