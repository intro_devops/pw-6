#!/bin/sh
token="my-token"

curl -H "Content-Type:application/json" http://localhost/api/v4/projects?private_token=$token -d "{ \"name\": \"project1\" }"
curl -H "Content-Type:application/json" http://localhost/api/v4/projects?private_token=$token -d "{ \"name\": \"project2\" }"
curl -H "Content-Type:application/json" http://localhost/api/v4/projects?private_token=$token -d "{ \"name\": \"project3\" }"
